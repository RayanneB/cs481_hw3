﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace cs481_hw2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }

        // Navigate to Page 1
        async void OnButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
        }

        // Navigate to Boomer Page
        async void OkBoomer(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new BoomerPage());
        }
        
        // Navigate to Millenial Page
        async void OkMillenial(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
        } 
    }
}
